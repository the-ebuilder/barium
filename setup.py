from pathlib import Path
import yaml

configfile=Path("config.yml")

print("This is the offical setup wizard for Barium IRC BOT\n---------------------------------------------------")
print("Searching for config file...")
if configfile.is_file():
    use_current_config=input("Config file found! Would you like to use the current config file? (Y/n): ")
    print(use_current_config.lower())
    if use_current_config.lower() in {'y','yes', ''}:
        print("Using current config file...")
        print("Setup is now complete! Run 'python3 irc.py' to start Barium IRC Bot.")
        exit()
    else:
        print("Generating a new config file...")
else:
    print("No config file was found! Generating a new file...")

print("Barium requires a pre-registered nickserv account with the bot's desired irc nick attached. Please ensure you have a nickserv account before continuing.")

nick = input("Enter the nick to use: ")
channel = input("Enter the channel to connect to: ")
commandkey = input("Enter the command key to use: ")
nickserv_email = input(f"Enter the email address of the nickserv account associated with {nick}: ")
nickserv_password = input(f"Enter the password of the nickserv account associated with {nick}: ")
owner_account = input("Enter the account of the owner of this instance of Barium: ")
server = input("Enter the server to connect: ")
port = input("Enter the port to use: ")
use_ssl = input("Should Barium use SSL? (True/False): ")

user_configs = {
    "nick": nick,
    "channel": channel,
    "commandkey": commandkey,
    "nickserv_email": nickserv_email,
    "nickserv_password": nickserv_password,
    "owner_account": owner_account,
    "server": server,
    "port": port,
    "use_ssl": use_ssl
}

with open(r'config.yml', 'w') as file:
    outputs = yaml.dump(user_configs, file)

print("Config file generated. Run 'python3 irc.py' to start Barium IRC Bot.")
print("---------------------------------------------------")
