import socket, ssl, irctokens, base64, yaml
from modules import *

with open("config.yml", "r") as stream:
    try:
        with open("config.yml", 'r') as stream:
            configuration = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print("ERROR: config.yaml not found")
        exit()

NICK = configuration["nick"]
CHAN = configuration["channel"]
NICKEMAIL = configuration["nickserv_email"]
NICKPASSWD = configuration ["nickserv_password"]
commandkey = configuration["commandkey"]
owner = configuration["owner_account"]
server = configuration["server"]
port = configuration["port"]
usessl = configuration["use_ssl"]
d = irctokens.StatefulDecoder()
e = irctokens.StatefulEncoder()
if usessl==True:
    context = ssl.create_default_context()
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s = context.wrap_socket(socket, server_hostname=server)
    s.connect((server, port))
else:
    s = socket.socket()
    s.connect((server, port))


def _send(line):
    print(f"> {line.format()}")
    e.push(line)
    while e.pending():
        e.pop(s.send(e.pending()))
_send(irctokens.build("CAP", ["REQ", "sasl account-tag"]))
_send(irctokens.build("AUTHENTICATE", ["PLAIN"]))
_send(irctokens.build("AUTHENTICATE", [base64.b64encode(f"{NICK}\x00{NICK}\x00{NICKPASSWD}".encode()).decode()]))
_send(irctokens.build("USER", ["username", "0", "*", "barium 0.1"]))
_send(irctokens.build("NICK", [NICK]))

exec_module={
    ping: "ping",
    sandcat: "sandcat",
    bahhumbug: "bahhumbug",
    eightball: "8ball",
    botsnack: "botnsack",
    tinyurl: "tinyurl",
    op: "op"
}

while True:
    lines = d.push(s.recv(1024))
    if lines is None:
        print("! disconnected")
        break
    for line in lines:
        print(line)
        if line.command in {"903", "904"}:
            _send(irctokens.build("CAP", ["END"]))
        if line.command == "PING":
            to_send = irctokens.build("PONG", [line.params[0]])
            _send(to_send)
        elif line.command == "001":
            to_send = irctokens.build("JOIN", [CHAN])
            _send(to_send)
        elif line.command == "PRIVMSG":
            if line.params[-1].startswith(commandkey):
                command=line.params[-1]
                if command==commandkey+"ping":
                    ping(line, _send)
                elif command==commandkey+"sandcat":
                    sandcat(line, _send)
                elif command==commandkey+"bahhumbug":
                    bahhumbug(line, _send)
                elif command[:6]==commandkey+"8ball":
                    eightball(line, _send)
                elif command==commandkey+"botsnack":
                    botsnack(line, _send)
                elif command==commandkey+"gentoo":
                    gentoo(line, _send)
                elif command==commandkey+"fedora":
                    fedora(line, _send)
                elif command==commandkey+"tux":
                    tux(line, _send)
                elif command[:8]==commandkey+"tinyurl":
                    tinyurl(line, _send, command)
                elif command[:3]==commandkey+"op":
                    op(line, _send, command, owner)
                elif command[:5]==commandkey+"deop":
                    deop(line, _send, command, owner)
                elif command[:6]==commandkey+"voice":
                    voice(line, _send, command, owner)
                elif command[:8]==commandkey+"devoice":
                    devoice(line, _send, command, owner)
