import irctokens, random
def ping(line, _send):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "pong"])
    _send(to_send)
def sandcat(line, _send):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "Jess is a sandcat"])
    _send(to_send)
    if line.hostmask.nickname == "jess":
        to_send2 = irctokens.build("PRIVMSG", [channel_reply, "Oh hey jess, meow"])
        _send(to_send2)
    elif line.hostmask.nickname == "oldfashionedcow":
        to_send2 = irctokens.build("PRIVMSG", [channel_reply, "Oh hey oldfashionedcow, moo"])
        _send(to_send2)
def bahhumbug(line, _send):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "It was a typo. Stuff happens."])
    _send(to_send)
def eightball(line, _send):
    questions = ["It is certain","Outlook good","You may rely on it","Ask again later","Concentrate and ask again","Reply hazy, try again","My reply is no","My sources say no"]
    answer = random.randint(1,8)
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, questions[answer-1]])
    _send(to_send)
def botsnack(line, _send):
    channel_reply = line.params[0]
    to_send = irctokens.build("PRIVMSG", [channel_reply, "Yum! Thank you!"])
    _send(to_send)
def gentoo(line, _send):
    channel_reply = line.params[0]
    _send(irctokens.build("PRIVMSG", [channel_reply,"         -/oyddmdhs+:."]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"     -odNMMMMMMMMNNmhy+-`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"   -yNMMMMMMMMMMMNNNmmdhy+-"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," `omMMMMMMMMMMMMNmdmmmmddhhy/`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," omMMMMMMMMMMMNhhyyyohmdddhhhdo`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,".ydMMMMMMMMMMdhs++so/smdddhhhhdm+`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," oyhdmNMMMMMMMNdyooydmddddhhhhyhNd."]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  :oyhhdNNMMMMMMMNNNmmdddhhhhhyymMh"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"    .:+sydNMMMMMNNNmmmdddhhhhhhmMmy"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       /mMMMMMMNNNmmmdddhhhhhmMNhs:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"    `oNMMMMMMMNNNmmmddddhhdmMNhs+`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  `sNMMMMMMMMNNNmmmdddddmNMmhs/."]))
    _send(irctokens.build("PRIVMSG", [channel_reply," /NMMMMMMMMNNNNmmmdddmNMNdso:`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"+MMMMMMMNNNNNmmmmdmNMNdso/-"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"yMMNNNNNNNmmmmmNNMmhs+/-`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"/hMMNNNNNNNNMNdhs++/-`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"`/ohdmmddhys+++/:.`"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  `-//////:--."]))
def fedora(line, _send):
    channel_reply = line.params[0]
    _send(irctokens.build("PRIVMSG", [channel_reply,"          /:-------------:\\"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       :-------------------::"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"     :-----------/shhOHbmp---:\\"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  /-----------omMMMNNNMMD  ---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  :-----------sMMMMNMNMP.    ---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply," :-----------:MMMdP-------    ---\\"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,",------------:MMMd--------    ---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":------------:MMMd-------    .---:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":----    oNMMMMMMMMMNho     .----:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":--     .+shhhMMMmhhy++   .------/"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-    -------:MMMd--------------:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-   --------/MMMd-------------;"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-    ------/hMMMy------------:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":-- :dMNdhhdNMMNo------------;"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":---:sdNMMMMNds:------------:"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":------:://:-------------::"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,":---------------------://"]))
def tux(line, _send):
    channel_reply = line.params[0]
    _send(irctokens.build("PRIVMSG", [channel_reply,"        #####"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       #######"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       ##O#O##"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"       #######"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"     ###########"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"    #############"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"   ###############"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"   ################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  #################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"#####################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"#####################"]))
    _send(irctokens.build("PRIVMSG", [channel_reply,"  #################"]))
