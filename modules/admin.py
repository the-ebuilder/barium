import irctokens

def op(line, _send, command, owner):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "+o", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        print(line.params[0])
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'op' command."])
        _send(to_send)
def deop(line, _send, command, owner):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "-o", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        print(line.params[0])
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'deop' command."])
        _send(to_send)
def voice(line, _send, command, owner):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "+v", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        print(line.params[0])
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'voice' command."])
        _send(to_send)
def devoice(line, _send, command, owner):
    if line.tags is not None and line.tags.get("account")==owner:
        channel_reply = line.params[0]
        arg = command.split()
        to_send = irctokens.build("MODE", [channel_reply, "-v", arg[1]])
        _send(to_send)
    else:
        channel_reply = line.params[0]
        print(line.params[0])
        to_send = irctokens.build("PRIVMSG", [channel_reply, f"{line.hostmask.nickname}: You are not authorized to perform 'devoice' command."])
        _send(to_send)
